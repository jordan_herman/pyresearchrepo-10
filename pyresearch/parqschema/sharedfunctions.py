# Parquet Functions
import subprocess
import pyresearch
def retSQL(hiveschema, tablename, location, partition, stype):
    # PUBLIC
    if stype == "CREATE":
        ret = """CREATE EXTERNAL TABLE %s (
%s
) %s
STORED AS PARQUET
LOCATION '%s'
""" % (tablename, hiveschema, partition, location)
    elif stype == "DROP":
        ret = "DROP TABLE %s" % tablename
    elif stype == "MSCK":
        ret = "MSCK REPAIR TABLE %s" % tablename
    elif stype == "SETMSCK":
        ret = "set hive.msck.path.validation=ignore"
    else:
        ret = "CONFUSION"

    return ret



def getparqinfo(in_hdfs_file):
    # PUBLIC
    parq_tools = '/opt/mapr/parquet/bin/parquet-tools'
    parq_out = subprocess.run([parq_tools, 'schema', in_hdfs_file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = parq_out.stdout
    err = parq_out.stderr
    return output.decode('UTF-8'), err.decode('UTF-8')



def parq2hive(parqstr, debug=False):
    # PUBLIC
    arfields = []
    tar = []
    nestlevel = 0
    nests = []
    for l in parqstr.split("\n"):
       # print(l.strip())
        if l.strip().find("message") == 0:
            pass # This is the header
        elif nestlevel == 0 and (l.strip().find("optional") == 0 or l.strip().find("required") == 0 or l.strip().find("repeated") == 0) and l.strip().find("group") < 0:
            if debug:
                print("Nest level: %s - required-repeated-optional-group - %s" % (nestlevel, l.strip()))
            arfields.append(l.strip())
        elif nestlevel == 0 and l.strip().find("group") >= 0:
            if debug:
                print("Nest level: %s - group - %s" % (nestlevel, l.strip()))
            # This is the start of a struct so
            nests = []
            nestlevel = 1
            nests.append(l.strip())
         #   print("nestlevel: %s - nests: %s" % (nestlevel, nests))
        elif nestlevel == 1 and (l.strip().find("optional") == 0 or l.strip().find("required") == 0 or l.strip().find("repeated") == 0) and l.strip().find("group") < 0:
            if debug:
                print("Nest level: %s - required-repeated-optional-group - %s" % (nestlevel, l.strip()))
            nests.append(l.strip())
        elif nestlevel == 1 and l.strip().find("group") >= 0:
            if debug:
                print("Nest level: %s - group - %s" % (nestlevel, l.strip()))
            nestlevel += 1
            nests.append([l.strip()])
        elif nestlevel == 2 and (l.strip().find("optional") == 0 or l.strip().find("required") == 0 or l.strip().find("repeated") == 0) and l.strip().find("group") < 0:
            nests[-1].append(l.strip())
        elif nestlevel == 2 and l.strip().find("group") >= 0:
            nestlevel += 1
            nests[-1].append([l.strip()])
        elif nestlevel == 3 and (l.strip().find("optional") == 0 or l.strip().find("required") == 0 or l.strip().find("repeated") == 0) and l.strip().find("group") < 0:
            nests[-1][-1].append(l.strip())
        elif nestlevel == 3 and l.strip().find("group") >= 0:
            nestlevel += 1
            nests[-1][-1].append([l.strip()])
        elif nestlevel == 4 and (l.strip().find("optional") == 0 or l.strip().find("required") == 0 or l.strip().find("repeated") == 0) and l.strip().find("group") < 0:
            nests[-1][-1][-1].append(l.strip())
        elif nestlevel == 4 and l.strip().find("group") >= 0:
            nestlevel += 1
            nests[-1][-1][-1].append([l.strip()])
        elif nestlevel > 0 and l.strip() == "}":
         #   print("nestlevel: %s - nests: %s" % (nestlevel, nests))
            if nestlevel == 4:
                nests[-1][-1][-1].append(l.strip())
            elif nestlevel == 3:
                nests[-1][-1].append(l.strip())
            elif nestlevel == 2:
                nests[-1].append(l.strip())
            elif nestlevel == 1:
                nests.append(l.strip())
            nestlevel = nestlevel - 1
            if nestlevel == 0:
                arfields.append(nests)
                nests = []
        elif nestlevel == 0 and l.strip() == "}":
            pass


    hiveret = []
    for i in arfields:
        if type(i) is str:
            hiveret.append(procstr(i))
        else:
            o = procgroup(i, False, debug)
            hiveret.append(o)
    hivestr = "\n".join(hiveret)[0:-1]

    return hivestr


def procgroup(g,bnest=False, debug=False):
    # PRIVATE
    retstr = ""
    astr = ""
   # print("Startingprocgroup: %s" % g)
    for i in g:
        if type(i) is str:
           # if i.find("subject") >= 0 or i.find("attributes") >= 0 or i.find("issuer") >= 0:
            if debug:
                print(i)
            s = i.split(" ")
            if len(s) == 4:
                r = s[0]
                t = s[1]
                n = s[2]
                u = s[3].replace(';', '')
            elif len(s) == 3:
                r = s[0]
                t = s[1]
                n = s[2].replace(';', '')
                u = ""
            elif len(s) == 1:
                r = s[0]
                t = ""
                n = ""
                u = ""
            if n in ['timestamp', 'date']:
                n = "`%s`" % n
            if debug:
                print("r: %s - t: %s - n: %s - u: %s" % (r, t, n, u))
            if r == "repeated" and t == "group":
                if bnest == True:
                    retstr = "%s:ARRAY<STRUCT<" % n
                else:
                    retstr = "%s ARRAY<STRUCT<" % n
                astr = ">>"
            elif t == "group":
                if bnest == True:
                    retstr = "%s:STRUCT<" % n
                else:
                    retstr = "%s STRUCT<" % n
                astr = ">"
            elif r == "repeated":
                if debug:
                    print("repeated!") 
                retstr = retstr + "%s: %s," % (n, rettype(r, t, u))
            elif r == "}":
                if retstr[-1] == ",":
                    retstr = retstr[0:-1]
                retstr = retstr + astr + ","
                astr = ""
            else:
                retstr = retstr + "%s: %s," % (n, rettype(r, t, u))
        else:
            tstr = ""
            tstr = procgroup(i, True, debug)
            retstr = retstr + tstr
    #print("Endingprocgroup")
    return retstr

def procstr(i):
    # PRIVATE
    retstr = ""
    s = i.split(" ")
    if len(s) == 4:
        r = s[0]
        t = s[1]
        n = s[2]
        u = s[3].replace(';', '')
    elif len(s) == 3:
        r = s[0]
        t = s[1]
        n = s[2].replace(';', '')
        u = ""
    if n in ['timestamp', 'date']:
        n = "`%s`" % n
    #print("r: %s - t: %s - n: %s - u: %s" % (r,t,n,u))
    retstr = "%s %s," % (n, rettype(r, t, u))

    return retstr


def rettype(r, t, u):
    # PRIVATE
    ret = ""
    if r == "repeated":
        mytype = rettype("", t, u)
      #  print(mytype)
        ret = "ARRAY <%s>" % mytype
    elif t == 'binary' and u == '(UTF8)':
        ret = "STRING"
    elif t == "binary" and u == "(ENUM)":
        ret = "STRING"
    elif t == "binary" and u == "":
        ret = "BINARY"
    elif t == "int64":
        ret = "BIGINT"
    elif t == 'double':
        ret = "DOUBLE"
    elif t == "int32":
        ret = "INT"
    elif t == "float": 
        ret = "FLOAT"
    elif t == "boolean":
        ret = "BOOLEAN"
    #print(ret)
    return ret

