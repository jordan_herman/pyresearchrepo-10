# Cloud Functions
from collections import OrderedDict
from pyresearch.cloud.sharedfunctions import *
import pyresearch

docs_dict = OrderedDict()


#   docs_dict[''] = {'fargs':'',
#               'fret': '',
#               'shortdesc': '',
#               'desc': ''
#               }


docs_dict['get_cloud_wc'] = {'fargs':'searchstr: a string sent to a mysql like statement looking at webcomponent name ',
          'fret': 'mydf: A data frame with the restults of the the web components query',
          'shortdesc': 'Get hosting provider web components based on the name',
          'desc': 'Using searchstr, search for webcomponents of the hosting_provider type.'
           }

docs_dict['get_azure_cidrs'] = {'fargs':'No Arguments',
          'fret': 'ret_cidrs, list of current cidrs based on providers endpoint',
          'shortdesc': 'Return all CIDRs that Microsoft associates with Azure',
          'desc': 'Return all CIDRs that Microsoft associates with Azure'
           }
docs_dict['get_amazon_cidrs'] = {'fargs':'No Arguments',
          'fret': 'ret_cidrs, list of current cidrs based on providers endpoint',
          'shortdesc': 'Return all CIDRs that Amazon associates with AWS',
          'desc': 'Return all CIDRs that Amazon associates with AWS'
           }
docs_dict['get_oracle_cidrs'] = {'fargs':'No Arguments',
          'fret': 'ret_cidrs, list of current cidrs based on providers endpoint',
          'shortdesc': 'Return all CIDRs that Oracle associates with Oracle Cloud',
          'desc': 'Return all CIDRs that Oracle associates with Oracle Cloud'
           }
docs_dict['get_ibm_cloud_cidrs'] = {'fargs':'No Arguments',
          'fret': 'ret_cidrs, list of current cidrs based on providers endpoint',
          'shortdesc': 'Return all CIDRs that IBM associates with IBM Cloud',
          'desc': 'Return all CIDRs that IBM associates with IBM Cloud'
           }
docs_dict['get_google_cidrs'] = {'fargs':'No Arguments',
          'fret': 'ret_cidrs, list of current cidrs based on providers endpoint',
          'shortdesc': 'Return all CIDRs that Google associates with GCS',
          'desc': 'Return all CIDRs that Google associates with GCS'
           }





def docs(funcname=None):
    pyresearch.docs(funcname, docs_dict)


