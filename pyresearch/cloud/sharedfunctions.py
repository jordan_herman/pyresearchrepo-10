#
import pandas as pd
import pyresearch
import dns.resolver
import json
import requests
import sys
import re
import netaddr
from bs4 import BeautifulSoup



def get_cloud_wc(searchstr):
    #PUBLIC 
    myipy = get_ipython()
    myipy.run_line_magic(u'mysql', 'set mysql_display_df False') 
    query = """select
webcomponentrule.rulekey, webcomponentrule.webcomponentruleid,  webcomponentrule.ruletype, webcomponentrule.statusCode, webcomponentrule.rulevalue, webcomponentrule.createdAt, webcomponentrule.updatedAt,
webcomponent.name, webcomponent.webcomponentid, webcomponent.version, webcomponent.type, webcomponent.md5, webcomponent.nameHash,
webcomponenttype.name as type_name,
webcomponentInfo.name as info_name, webcomponentinfo.displayname, webcomponentinfo.url, webcomponentinfo.description, webcomponentinfo.statuscode as info_status_code
from 
webcomponentrule join webcomponent using(webcomponentid) 
join webcomponenttype on (HEX(webcomponent.type) = HEX(webcomponenttype.code))
LEFT OUTER JOIN webcomponentinfo on webcomponent.webcomponentinfoid = webcomponentinfo.webcomponentinfoid
where
webcomponentrule.ruletype='J'
and webcomponent.name like '%s'""" % searchstr
    
    other_query = "select webcomponentrule.rulekey, webcomponent.name from webcomponentrule join webcomponent using(webcomponentid) where ruletype='J' and statusCode='Y'"
        
    myipy.run_cell_magic(u'mysql', "", query)
    mydf = myipy.user_ns['prev_mysql']
    myipy.run_line_magic(u'mysql', 'set mysql_display_df True')
    
    return mydf


def get_alibaba_cloud_cidrs(retval="cidrs"):
    print("*** No Alibaba Cloud provided list found - 2019-12-19 - JO ***")
    return []


def get_ibm_cloud_cidrs(retval="cidrs"):
    all_cidrs = []
    regions = {}
    services = {}
    ignore_sections = ['section-ibm-cloud-ip-ranges', 'section-vulnerability-scans', 'section-backend-private-network', 'section-service-network-on-backend-private-network-', 'section-ssl-vpn-network-on-backend-private-network-',
                       'section-legacy-networks', 'section-ssl-vpn-datacenters', 'section-ssl-vpn-pops', 'section-pptp-vpn-datacenters', 'section-pptp-vpn-pops', 'section-red-hat-enterprise-linux-server-requirements']
    url = "https://cloud.ibm.com/docs-content/v1/content/fbbed92dcb83d1d001bbc4fc7390af9c6f0a2b2f/infrastructure/hardware-firewall-dedicated/ips.html?locale=en"
    response = requests.get(url)

    soup = BeautifulSoup(response.text, "html.parser")
 
    for sid in soup.findAll("section"):
        if sid['id'] not in ignore_sections:
            service = sid['id'].replace("section-", "")
            for tr in sid.findAll("tr"):
                td = tr.findAll("td")
                row = [i.text for i in td]
                if len(row) == 5:
                    dc = row[0]
                    city = row[1]
                    state = row[2]
                    country = row[3]
                    cidrs = row[4]
                    for cidr in cidrs.split(","):
                        mycidr = cidr.strip()
                        ourcidrs = mycidr.split(" ")
                        for c in ourcidrs:
                            if pyresearch.util.isRoutableCIDR(c):
                                all_cidrs.append(c)
                                if dc in regions:
                                    regions[dc].append(c)
                                else:
                                    regions[dc] = [c]
                                if service in services:
                                    services[service].append(c)
                                else:
                                    services[service] = [c]
    ret_cidrs = list(set(all_cidrs))
    if retval == "cidrs":
        return ret_cidrs
    elif retval == "services":
        return services
    elif retval == "regions":
        return regions

def get_oracle_cidrs(retval='cidrs'):
    #PUBLIC 
    # Returns all CIDRs
    regions = {}
    services = {}
    url = "https://docs.cloud.oracle.com/iaas/tools/public_ip_ranges.json"
    response = requests.get(url)
    if response.status_code not in range(200, 299):
        raise Exception("Oracle Parse Failure:", response.content)
    loaded = json.loads(response.content)
    ret_cidrs = [cidr['cidr'] for reg in loaded['regions'] for cidr in reg['cidrs']]
    
    
    for r in loaded['regions']:
    # Handle regions first
        if r['region'] not in regions:
            mycidrs = [x['cidr'] for x in r['cidrs']]
            regions[r['region']] = mycidrs
        else:
            mycidrs = [x['cidr'] for x in r['cidrs']]
            regions[r['region']] += mycidrs
    # Now Services
        for c in r['cidrs']:
            this_cidr = c['cidr']
            for t in c['tags']:
                if t in services:
                    services[t].append(this_cidr)
                else:
                    services[t] = [this_cidr]
        


    if retval == 'cidrs':
        return ret_cidrs
    elif retval == 'regions':
        return regions
    elif retval == 'services':
        return services
    elif retval == "all":
        return loaded
    else:
        print("retval %s unknown, returning cidrs" % retval)
        return ret_cidrs


def get_amazon_cidrs(retval='cidrs'):
    #PUBLIC 
    # Returns all CIDRs
    regions = {}
    services = {}

    url = "https://ip-ranges.amazonaws.com/ip-ranges.json"
    response = requests.get(url)
    if response.status_code not in range(200, 299):
        raise Exception("Amazon Parse Failure:", response.content)
    loaded = json.loads(response.content)
    cidrs = list(set([x['ip_prefix'] for x in loaded['prefixes'] if x['service'] == 'AMAZON']))
    ret_cidrs = [x for x in cidrs if pyresearch.util.isCIDR(x)]

    for p in loaded['prefixes']:
        if p['region'] in regions:
            regions[p['region']].append(p['ip_prefix'])
        else:
            regions[p['region']] = [p['ip_prefix']]
        if p['service'] in services:
            services[p['service']].append(p['ip_prefix'])
        else:
            services[p['service']] = [p['ip_prefix']]

    if retval == 'cidrs':
        return ret_cidrs
    elif retval == 'all':
        return loaded
    elif retval == 'regions':
        return regions
    elif retval == 'services':
        return services
    else:
        print("Unknown retval: %s - returning all cidrs" % retval)
        return ret_cidrs


def get_google_cidrs(retval='cidrs'):
    #PUBLIC 
    base_host = '_cloud-netblocks.googleusercontent.com'
    ipv4s, ipv6s = google_recurse(base_host)
    ret_cidrs = [x for x in ipv4s if pyresearch.util.isCIDR(x)]
    if retval == 'cidrs':
        return ret_cidrs
    else:
        print("Unknown retval for Google Cloud (only supports cidrs): {0} - Returning Cidrs".format(retval))
        return ret_cidrs

def google_recurse(start_host):
    ipv4 = []
    ipv6 = []
    records = [str(x) for x in dns.resolver.query(start_host, 'TXT')]
    for rec in records:
        incs = []
        ipv4s = []
        ipv6s = []
        incs = re.findall(r"include:([^ ]+)", rec)
        for inc in incs:
            new4s = []
            new6s = []
            new4s, new6s = google_recurse(inc)
            ipv4 += new4s
            ipv6 += new6s

        ipv4s = re.findall(r"ip4:([^ ]+)", rec)
        ipv4 += ipv4s
        ipv6s = re.findall(r"ip6:([^ ]+)", rec)
        ipv6 += ipv6s
    return ipv4, ipv6


def get_azure_cidrs(retval='cidrs'):
    regions = {}
    services = {}
    all_cidrs = []
    full_raw = {}
    full_data = []
    urls = ['https://www.microsoft.com/en-us/download/confirmation.aspx?id=56519', 'https://www.microsoft.com/en-us/download/confirmation.aspx?id=57063', 'https://www.microsoft.com/en-us/download/confirmation.aspx?id=57064', 'https://www.microsoft.com/en-us/download/confirmation.aspx?id=57062']
    ses = requests.session()
    full_data = []
    for url in urls:
        response = ses.get(url)
        response_str = response.content.decode('utf-8')
        m = re.search(r'"(https:\/\/download\.microsoft\.com\/download\/[^"]+)"', response_str)
        if m:
            url = m[1]
            if url.find("ServiceTags") >= 0:
                json_response = ses.get(url)
                r_json = json.loads(json_response.text)
                full_raw[url] = r_json
                cloud_val = r_json['cloud']
                vals = r_json['values']
                output = []
                for v in vals:
                    if 1 == 1:
                        aid = v['id']
                        if aid.find(".") >= 0:
                            lowerid = aid.split(".")[0].lower()
                            lowerregion = aid.split(".")[1].lower()
                        else:
                            lowerid = aid.lower()
                            lowerregion = ""
                        p = v['properties']
                        region = p['region']
                        platform = p['platform']
                        systemService = p['systemService']
                        addrs = p['addressPrefixes']
                        for a in addrs:
                            full_data.append({'cloud': cloud_val, 'id': aid, 'lowerid': lowerid, 'lowerregion': lowerregion, 'region': region, 'platform': platform, 'systemService': systemService, 'ip_prefix': a})
            else:
                print("Weird URL, not getting: %s" % url)
        else:
            print("No Match")
    for a in full_data:      
        myregion = a['region']
        myservice = a['systemService']
        mycidr = a['ip_prefix']
        all_cidrs.append(mycidr)
        if not pyresearch.util.isRoutableCIDR(mycidr):
            print("Skipping: %s" % mycidr)
            next
        if myservice == '':
            myservice = "Unspecified"
        if myregion == '':
            myregion = "Unspecified"
        
        if myregion in regions:
            if mycidr not in regions[myregion]:
                regions[myregion].append(mycidr)
        else:
            regions[myregion] = [mycidr]
    
        if myservice in services:
            if mycidr not in services[myservice]:
                services[myservice].append(mycidr)
        else:
            services[myservice] = [mycidr]            
            
    all_cidrs = list(set(all_cidrs))         

    if retval == 'cidrs':
        return all_cidrs
    elif retval == 'regions':
        return regions
    elif retval == 'services':
        return services
    elif retval == 'full_parsed':
        return full_data
    elif retval == 'raw':
        return full_raw
    else:
        print("Unknown retval: {0} returning full cidrs".format(retval))
        return cidrs
        
    return full_data


def write_wcs_update(base_dir, provider):
    import os
    if not os.path.isdir(base_dir):
        print("Base Directory: %s does not exist - please create first" % base_dir)
        return None
    our_path = "%s/%s" % (base_dir, provider)
    if not os.path.isdir(our_path):
        os.mkdir(our_path)
    
    tsv_out, disable_sql = pyresearch.cloud.update_wcs(provider)

    if tsv_out is not None:
        if os.path.exists(our_path + "/enable_rules.sql"):
            os.remove(our_path + "/enable_rules.sql")
        if os.path.exists(our_path + "/disable_rules.sql"):
            os.remove(our_path + "/disable_rules.sql")
        if os.path.exists(our_path + "/cidrs_add.tsv"):
            os.remove(our_path + "/cidrs_add.tsv")
        if disable_sql != "":
            f = open(our_path + "/disable_rules.sql", "w")
            f.write(disable_sql + "\n")
            f.close()
        if len(tsv_out) > 0:
            f = open(our_path + "/cidrs_add.tsv", 'w')
            for x in tsv_out:
                f.write("\t".join(x) + "\n")
            f.close()
    else:
        print("Error Returned: %s" % disable_sql)


def update_wcs(provider):

    cloud_providers = {}
    cloud_providers['Amazon'] = ['Amazon%', pyresearch.cloud.get_amazon_cidrs()]
    cloud_providers['Azure'] = ['Microsoft Azure%', pyresearch.cloud.get_azure_cidrs()]
    cloud_providers['Google'] = ['Google%', pyresearch.cloud.get_google_cidrs()]
    cloud_providers['IBM'] = ['IBM Cloud%', pyresearch.cloud.get_ibm_cloud_cidrs()]
    cloud_providers['Oracle'] = ['Oracle%', pyresearch.cloud.get_oracle_cidrs()]


    if provider not in cloud_providers:
        print("%s is not in our current supported providers - exiting" % provider)
        return None, None, "Err: Provider not found"

    cur_web_components = pyresearch.cloud.get_cloud_wc(cloud_providers[provider][0])
    cur_cidrs = cloud_providers[provider][1]

    wcs = list(set(cur_web_components['webcomponentid'].tolist()))
    if len(wcs) == 1:
        w_id = wcs[0]
    else:
        return None, None, "Multiple WC IDs returned - Not Outputting: %s " % wcs

    wc_names = list(set(cur_web_components['name'].tolist()))
    if len(wc_names) == 1:
        wc_name = wc_names[0]
    else:
        return None, None, "Multiple WC Names returned - Not Outputting: %s" % wc_names

   
    wc_dict = cur_web_components.to_dict('records')
    enabled_wc = [[x['webcomponentruleid'], x['rulekey']] for x in wc_dict if x['statusCode'] == 'Y']
    enabled_wc_cidrs = [x[1] for x in enabled_wc]
    disabled_wc = [[x['webcomponentruleid'], x['rulekey']] for x in wc_dict if x['statusCode'] == 'N']
    disabled_wc_cidrs = [x[1] for x in disabled_wc]

    # Get web component rule ids to disable because they no longer exist in the providers list
    wc_to_disable = [x[0] for x in enabled_wc if x[1] not in cur_cidrs]
    print("Number of web components rule IDs disable for %s in webcomponent_id: %s: %s" % (provider, w_id, len(wc_to_disable)))
    print("")

    # This is a list of webcomponentruleids that are disabled now, and do match the current list (re-enable)
    wc_to_enable = [x[0] for x in disabled_wc if x[1] in cur_cidrs]
    cidrs_to_re_enable = [x for x in disabled_wc_cidrs if x in cur_cidrs]
    print("Number of web components rule IDs to re-enable for %s in webcomponent_id %s:  %s" % (provider, w_id, len(wc_to_enable)))
    print("")

    # These are the cidrs to add because they do not exist
    cidrs_to_add = [x for x in cur_cidrs if x not in enabled_wc_cidrs and x not in disabled_wc_cidrs]
    add_len = len(cidrs_to_add)
    # This also adds the cidrs to renable to the list so they are included for the groovy script
    cidrs_to_add = cidrs_to_add + cidrs_to_re_enable

    print("Number of cidr web component rules to create for %s in webcomponent_id %s: %s (%s total with reenable)" % (provider, w_id, add_len, len(cidrs_to_add)))
    print("")


    #SQL to disable webcomponents
    if len(wc_to_disable) > 0:
        disable_sql = "update webcomponentrule set statuscode = 'N', updatedat = now() where webcomponentruleid in(%s) and ruleType='J'" % ",".join([str(x) for x in wc_to_disable])
    else:
        disable_sql = ""

   #Get TSV output
    add_wcs_out = []
    for x in cidrs_to_add:
        add_wcs_out.append(["CIDR EXACT", x, "", wc_name, "", "Hosting Provider"])

    return add_wcs_out, wc_to_disable, "Success"


def checkout_cloud_wcs(provider, dry_run = False):
    import os
    import requests
    user_key = os.environ.get('JUPYTERHUB_USER')
    rusers = {}
    rusers['jomernik'] = 8906
    rusers['yonathan'] = 4702
    rusers['ahunt'] = 1368
    rusers['sginty'] = 3123
    rusers['mihm'] = 9370
    rusers['mlindsey'] = 5913
    rusers['jherman'] = 4011
    rusers['spon'] = 4082
    
    add_base_url = "http://sf.vip.riskiq:8080/crawlmgr/service/classifier/add-web-component-rule"
    disable_base_url = "http://sf.vip.riskiq:8080/crawlmgr/service/classifier/disable-web-component-rule"
    if user_key not in rusers:
        print("The user identified (%s) was not found in authorized users, please talk to someone" % user_key)
        return None
    else:
        userid = rusers[user_key]
        
        
    print("Syncing %s cloud web components - User: %s" % (provider, user_key))
    print("")
    add_cidrs, disable_wcs, str_result = pyresearch.cloud.update_wcs(provider)
    if add_cidrs is None:
        print("Error returned - Not Adding: %s" % str_result)
        return None
    else:
        if len(add_cidrs) > 0:
            print("Adding %s cidrs for %s" % (len(add_cidrs), provider))
            if dry_run == True:
                print("Dry run only - not adding via API")
            for c in add_cidrs:
                
                qstr = "?name=%s&version=&type=%s&ruleKey=%s&ruleValue=&ruleType=%s&userID=%s" % (requests.utils.quote(c[3]), requests.utils.quote(c[5]), requests.utils.quote(c[1]), requests.utils.quote(c[0]), userid)
                full_url = add_base_url + qstr
                if dry_run:
                    print(full_url)
                else:
                    res = requests.get(full_url)
                    if res.status_code != 200: 
                        print("Error on %s - Code: %s - URL: %s" % (c[1], res.status_code, full_url))
                    else:
                        print("Added %s with status code 200" % c[1])
        full_url = ""                        
        if len(disable_wcs) > 0:
            print("Disabling %s wec components for %s" % (len(disable_wcs), provider))
            if dry_run == True:
                print("Dry run only - not adding via API")
            for w in disable_wcs:
                qstr = "?ruleID=%s&userID=%s" % (w, userid)
                full_url = disable_base_url + qstr
                if dry_run:
                    print(full_url)
                else:
                    res = requests.get(full_url)
                    if res.status_code != 200:
                        print("Error on %s - Code: %s - URL: %s" % (w, res.status_code, full_url))
                    else:
                        print("Disabled Web Component %s with status code 200" % w)


#name=Amazon%20Web%20Services&version=&type=Hosting%20Provider&ruleKey=150.222.85.0/24&ruleValue=&ruleType=CIDR%20EXACT&userID=4082
    
    
def checkout_cloud_wcs_old(provider, dry_run = False):
    import os
    import requests
    user_key = os.environ.get('JUPYTERHUB_USER')
    rusers = {}
    rusers['jomernik'] = 8906
    rusers['yonathan'] = 4702
    rusers['ahunt'] = 1368
    rusers['sginty'] = 3123
    rusers['mihm'] = 9370
    rusers['mlindsey'] = 5913
    rusers['jherman'] = 4011
    rusers['spon'] = 4082
    
    base_url = "http://sf.vip.riskiq:8080/crawlmgr/service/classifier/add-web-component-rule"
    
    if user_key not in rusers:
        print("The user identified (%s) was not found in authorized users, please talk to someone" % user_key)
        return None
    else:
        userid = rusers[user_key]
        
        
        
    myipy = get_ipython()
    myipy.run_line_magic(u'mysql', 'set mysql_display_df False')
    myipy.run_cell_magic(u'mysql','', 'show grants')
    try:
        results = myipy.user_ns['prev_mysql']
        colname = results.columns[0]
        conuser = colname.split("@")[0].replace("Grants for ", "")
        if conuser != "linkco" and dry_run == False:
            print("Not connected with read/write user - Not going on")
            myipy.run_line_magic(u'mysql', 'set mysql_display_df True')    
            return None
        else:
            pass
    except:
        print("Mysql is not connected - Please connect with Read/Write User")
        myipy.run_line_magic(u'mysql', 'set mysql_display_df True')    
        return None
    myipy.run_line_magic(u'mysql', 'set mysql_display_df True')    
    print("Syncing %s cloud web components - User: %s" % (provider, user_key))
    print("")
    add_cidrs, disable_wcs, str_result  = pyresearch.cloud.update_wcs(provider)
    if add_cidrs is None:
        print("Error returned - Not Adding: %s" % str_result)
        return None
    else:
        if len(add_cidrs) > 0:
            print("Adding %s cidrs for %s" % (len(add_cidrs), provider))
            if dry_run == True:
                print("Dry run only - not adding via API")
            for c in add_cidrs:
                
                qstr = "?name=%s&version=&type=%s&ruleKey=%s&ruleValue=&ruleType=%s&userID=%s" % (requests.utils.quote(c[3]), requests.utils.quote(c[5]), requests.utils.quote(c[1]), requests.utils.quote(c[0]), userid)
                full_url = base_url + qstr
                if dry_run:
                    print(full_url)
                else:
                    res = requests.get(full_url)
                    if res.status_code != 200: 
                        print("Error on %s - Code: %s - URL: %s" % (c[1], res.status_code, full_url))
                    else:
                        print("Added %s with status code 200" % c[1])

    #SQL to disable webcomponents
    
    if len(disable_wcs) > 0:
        disable_sql = "update webcomponentrule set statuscode = 'N', updatedat = now() where webcomponentruleid in(%s) and ruleType='J'" % ",".join([str(x) for x in disable_wcs])
    else:
        disable_sql = ""                        
    if disable_sql != "":
        if dry_run == True:
            print("Sql to run to disable webcomponents (ensure you are connected via read/write account) (Not running due to dry_run)")
            print("")
            print(disable_sql)
            print("")
        else:
            myipy.run_line_magic(u'mysql', 'set mysql_display_df False')
            print("Running: %s" % disable_sql)
            myipy.run_cell_magic(u'mysql','', disable_sql)
            myipy.run_line_magic(u'mysql', 'set mysql_display_df True')

#name=Amazon%20Web%20Services&version=&type=Hosting%20Provider&ruleKey=150.222.85.0/24&ruleValue=&ruleType=CIDR%20EXACT&userID=4082
    
    
